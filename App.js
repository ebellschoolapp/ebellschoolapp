/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
  View,
  StatusBar,
  Image,
  StyleSheet,
  Dimensions,
  ImageBackground
} from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import AsyncStorage from '@react-native-community/async-storage';
import { AppContainer } from './src/Components/Router';
import { BoldText, LightText } from './src/Components/styledTexts';

const {height, width} = Dimensions.get('screen')

export default class App extends Component {

  constructor(props){
    super(props);
    this.state = {
      showRealApp: false,
      isChecked: false
    }
  }
async componentDidMount(){
    console.disableYellowBox = true;
    // const value = await AsyncStorage.getItem('@notFirstRun')
    AsyncStorage.getItem('@notFirstRun').then((value) =>{
      this.setState({isChecked: true})
    if(value === 'ok') {
      this.setState({ showRealApp:true})
    }
  })
  }
  

  _renderItem = ({ item, dimensions }) => (
    <ImageBackground
      source={item.image}
      style={{ height: height, width: width}}
      resizeMode={'cover'}
    >
      <View style={{ flex: 1, marginHorizontal: 15, }}>
      <View style={{flex: .1}}/>
      <View style={{ flex: 1, alignItems:'center', justifyContent:'flex-start'}}>
        <Image
          source={require('./src/assets/stu.png')}
          style={{ tintColor:'#fff', height: 60, width:60}}
        />
        </View>
        <View style={{ flex: 1, alignItems:'center', justifyContent:'flex-end'}}>
        <BoldText style={{ color: '#fff', textAlign:'center', fontSize: 26}}>Welcome</BoldText>
        <BoldText style={{ color: '#ff732e', textAlign:'center', marginVertical: 10}}>My School app</BoldText>
        <LightText style={{ color: '#fff', textAlign:'center', fontSize: 18}}>{item.text}</LightText>
      </View>
      <View style={{flex: .5}}/>
      </View>
      </ImageBackground>
  );

  _onDone = () => {
    AsyncStorage.setItem('@notFirstRun', 'ok')
    this.setState({ showRealApp: true });
  }
  _onSkip = () => {
    AsyncStorage.setItem('@notFirstRun', 'ok')
    this.setState({ showRealApp: true });
  };

  render() {
    if(this.state.isChecked){
    if (this.state.showRealApp) {
      return (
        // <SafeAreaView style={{flex: 1, backgroundColor:'#22bcb5'}}>
        <View style={{ flex: 1}}>
          <StatusBar backgroundColor="silver"/>
          <AppContainer />
          </View>
        // </SafeAreaView>
      );
    } else {
      return (
        <AppIntroSlider
          slides={slides}
          renderItem={this._renderItem}
          onDone={this._onDone}
          showSkipButton={true}
          onSkip={this._onSkip}
          activeDotStyle={{backgroundColor: '#ff732e'}}
          buttonTextStyle={{color:'#ff732e'}}
        />
      )
    }}else{
      return (
        <View style={{ flex:1}}>
           {/* <Image
            source={require('./src/assets/LoadingPage.png')}
            style={{height: height, width: width}}
            resizeMode={'cover'}
           /> */}
        </View>
      )
    }

  }
};

const styles = StyleSheet.create({
  image: {
    width: 200,
    height: 200,
  },
  text: {
    color: 'gray',
    fontSize: 20,
  },
  title: {
    fontSize: 28,
    fontWeight: 'bold',
    color: 'gray',
    backgroundColor: 'transparent',
    textAlign: 'center',
    marginTop: 16,
  },
});
 
const slides = [
  {
    key: 's1',
    text: `We Connect All Elements of School and Manage Your Academic Operation`,
    title: 'We happy with e-bell',
    titleStyle: styles.title,
    textStyle: styles.text,
    image: require('./src/assets/bg1.png'),
    imageStyle: styles.image,
    backgroundColor: '#fff',
  },
  {
    key: 's1',
    text: `We Make You More Accessible and Hassel Free With One Click Academic Solutions.`,
    title: 'We are feel your feeling',
    titleStyle: styles.title,
    textStyle: styles.text,
    image: require('./src/assets/bg2.png'),
    imageStyle: styles.image,
    backgroundColor: '#fff',
  },
  {
    key: 's2',
    title: `Monitor All the Activities of Children's as-Home Work, Daily Attendance, Success Reports, Free Structure That Makes You Tension Free by Live Chat With Teacher's and Facilities.`,
    titleStyle: styles.title,
    textStyle: styles.text,
    text: 'we makes you more accessible and Hassel free with one click academic solutions.',
    image: require('./src/assets/bg3.png'),
    imageStyle: styles.image,
    backgroundColor: '#fff',
  },
  {
    key: 's3',
    title: 'Educating all students for success in a changing world',
    titleStyle: styles.title,
    textStyle: styles.text,
    text: `we makes you more accessible and Hassel free with one click academic solutions.`,
    image: require('./src/assets/bg4.png'),
    imageStyle: styles.image,
    backgroundColor: '#fff',
  }
];