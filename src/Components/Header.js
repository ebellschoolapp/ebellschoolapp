import React, { Component } from 'react';
import {
  View,
  TouchableOpacity,
  Image,
  Platform,
  Dimensions
} from 'react-native';
import { DrawerActions } from 'react-navigation-drawer';
import colors from '../styles/colors';
import {MediumText} from '../Components/styledTexts';

export default class Header extends Component {
  constructor(props){
    super(props)
    this.state={
      width:0
    }
  }
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: colors.appColor }}>

        <View style={[{ flex: 1, flexDirection: 'row', justifyContent: 'flex-end' }, Platform.OS === "android" ? { alignItems: 'center', } : { alignItems: 'flex-end', marginBottom: 5 }]}>
          <View style={{ width: 50, alignItems: 'center', justifyContent: 'center' }}>
            {this.props.leftNavigation ? (
              <TouchableOpacity
                onPress={() => {
                  this.props.leftNavigation.goBack(null);
                }}
              >
                <Image
                  source={require('../assets/back.png')}
                  style={{ height: 25, width: 25, marginBottom: 5, tintColor:'#fff' }}
                  resizeMode={'contain'}
                />
              </TouchableOpacity>
            ) : (null)}
            {/* Drawer           */}
            {this.props.drawer ? (
              <TouchableOpacity
                onPress={() => {
                  this.props.drawer.dispatch(DrawerActions.toggleDrawer())
                }}
              >
                <Image
                  source={require('../assets/menu.png')}
                  style={{ height: 20, width: 25, marginBottom: 5, tintColor:'#fff' }}
                  resizeMode={'contain'}
                />
              </TouchableOpacity>
            ) : (null)}

          </View>
          <TouchableOpacity disabled={true} style={{ flex: 1,  flexDirection:'row', alignItems: 'center', marginBottom: 5 }} disabled={this.props.onDropDownPress ? false : true} onPress={this.props.onDropDownPress}
          onLayout={(event) => {
            var {x, y, width, height} = event.nativeEvent.layout;
            this.setState({width: width})
          }} 
          >
            <MediumText style={{ fontSize: 16, maxWidth: this.state.width-30, color:'#fff'  }}>{this.props.value}</MediumText>
          </TouchableOpacity>
          
          <View style={{ flex: 1, flexDirection: 'row', paddingRight: 8, justifyContent:'flex-end' }}>
            {this.props.onPressOption &&
              <TouchableOpacity style={{ marginHorizontal: 3, }}
                onPress={this.props.onPressOption}
              >
                <Image
                  source={require('../assets/option.png')}
                  style={{ height: 20, width: 25, marginBottom: 5, tintColor: '#fff' }}
                  resizeMode={'contain'}
                />
              </TouchableOpacity>
            }
             
          </View>

        </View>
        <View style={{ height: 2, backgroundColor: colors.gray }} />
      </View>
    )
  }
}

