import React from 'react';
import {Image} from 'react-native';
import {
    createAppContainer,
} from 'react-navigation';
import {createBottomTabNavigator } from 'react-navigation-tabs';
import {createStackNavigator} from 'react-navigation-stack';
import {createDrawerNavigator} from 'react-navigation-drawer';
import colors from '../styles/colors';
import SelectUserType from '../Screens/SelectUserType';
import Login from '../Screens/Login';
import SignUp from '../Screens/SignUp';
import SideMenu from '../Screens/SideMenu';
import Home from '../Screens/Home';
import Attendance from '../Screens/Attendance';
import StudentProfile from '../Screens/StudentProfile';

export const loginNavigationOptions = createStackNavigator(
    {
       Login: { screen: Login},
        SignUp:{ screen: SignUp}, 
    },
    { headerMode: 'none'}
);

export const homeNavigationOptions = createStackNavigator(
    {
       Home: { screen: Home },
       Attendance: Attendance,
       StudentProfile:StudentProfile,
    },
    {  headerMode: 'none' }
);

  
export const bottomTabNavigator = createBottomTabNavigator(
    {
        Home:{
            screen: homeNavigationOptions,
            navigationOptions:{
                tabBarIcon: ({tintColor}) =>{
                    return(
                        <Image
                        source = {require('../assets/Home.png')}
                        style = {{tintColor:tintColor, height: 30, width: 30}}
                        />
                    )
                }
            }
        },
        'Behavior Notes':{
            screen: homeNavigationOptions,
            navigationOptions:{
                tabBarIcon: ({tintColor}) =>{
                    return(
                    <Image
                        source = {require('../assets/Home.png')}
                        style = {{tintColor:tintColor, height: 25, width: 30}}
                        />                    )
                }
            }
        },
        'Circulars':{
            screen: homeNavigationOptions,
            navigationOptions:{
                tabBarIcon: ({tintColor}) =>{
                    return(
                    <Image
                        source = {require('../assets/Home.png')}
                        style = {{tintColor:tintColor, height: 30, width: 30}}
                        />                    )
                }
            }
        },
        'Homework':{
            screen: homeNavigationOptions,
            navigationOptions:{
                tabBarIcon: ({tintColor}) =>{
                    return(
                        <Image
                        source = {require('../assets/Home.png')}
                        style = {{tintColor:tintColor, height: 30, width: 30}}
                        />
                    )
                }
            }
        }
    },
    {
        initialRouteName: 'Home',        
        tabBarOptions: {
            activeTintColor: colors.appColor,
            inactiveTintColor: colors.gray,
            showLabel: true,
            showIcon: true,
            tabBarPosition: 'bottom',
            labelStyle: {
                fontSize: 12,
            },
            iconStyle:{
                width: 30,
                height: 30
            },
            style: {
                backgroundColor: 'rgb(245,245,245)',
                borderBottomColor: '#ededed',
                alignItems: 'center',
                justifyContent: 'center',
                alignSelf: 'center',
            },
            lazy: true,
            indicatorStyle: '#fff',
        }
    }
  );
  export default drawerNavigation = createDrawerNavigator({
    bottomTabNavigator:{ screen: bottomTabNavigator },
  },{
    initialRouteName: 'bottomTabNavigator',
    contentComponent: SideMenu,
    drawerWidth: 300,
    contentOptions: {
      activeTintColor: '#e91e63',
    },
})


export const appNavigationOptions = createStackNavigator(
    {
        SelectUserType:{ screen: SelectUserType },
        loginNavigationOptions:{ screen:loginNavigationOptions },
        drawerNavigation:{ screen: drawerNavigation}

    },{
        headerMode: 'none'
    }
);

export const AppContainer = createAppContainer(appNavigationOptions);