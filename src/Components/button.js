import React, { Component } from "react";
import {
  TouchableOpacity,
  Image,
  View
} from "react-native";
import { BoldText, LightText } from '../Components/styledTexts';
import colors from '../styles/colors';

export class Button extends Component {
  render() {
    return (

      <TouchableOpacity
        style={[{
          height: 50,
          borderRadius: 5,
          backgroundColor: this.props.color,
          marginHorizontal: 50,
          marginVertical: 20,
          alignItems:'center',
          justifyContent:'center',

        }, this.props.style]}
        onPress={this.props.onPress}
      >

          {this.props.Light ? (
            <LightText style={[{ color: '#fff', fontSize: 16 }, this.props.textStyle]}>{this.props.value}</LightText>
          ) : (
              <BoldText style={[{ color: '#fff', }, this.props.textStyle]}>{this.props.value}</BoldText>
            )
          }

      </TouchableOpacity>
    )
  }
};

export class RadioButton extends Component {
  render(){
    return(
      <TouchableOpacity style={[{ flexDirection: 'row', height: 40, justifyContent:'center', alignItems:'center' }, this.props.containerStyle]}
        onPress = {this.props.onPress}
      >
          {/* <Image
            source = { (this.props.isSelected) ? require('../assets/radio_button.png') : require('../assets/radio_button_unchecked.png')}
            style={{ tintColor:'#FFBD4B'}}
          /> */}
          <LightText style={{ marginLeft: 5 }}>{this.props.value}</LightText>
      </TouchableOpacity>
    )
  }
}
