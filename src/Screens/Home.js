import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
  Dimensions,
  Image,
  ScrollView
} from "react-native";
import Loader from '../Components/Loader';
import Header from '../Components/Header';
import { BoldText, LightText } from '../Components/styledTexts';
import colors from "../styles/colors";

const { height, width } = Dimensions.get('screen');

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }

  componentWillMount() {
  }

  render() {
    return (
      <View style={{ flex: 1 }}>

        <View style={{ flex: .1 }}>
          <Header
             navigation={this.props.navigation}
             drawer={this.props.navigation}
             onPressOption={()=>alert('')}
              value={'Home'}
          />
        </View>
        <View style={{ flex: 1 }}>
            <View style={{padding: 10, margin: 10, justifyContent: 'center', borderWidth: 2, borderRadius: 3, borderColor:colors.gray }}>
              <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'center' }}
              onPress={()=> this.props.navigation.navigate('StudentProfile')}
              >
                <View style={{ marginLeft: 5, alignItems: 'center', justifyContent: 'center' }}>
                  <Image
                    source={require('../assets/user.png')}
                    style={{ height: 70, width: 70, borderRadius: 40, borderWidth: 1 }}
                  />
                </View>
                <View style={{ flex: 1, marginLeft: 15 }}>
                  <BoldText style={{ fontWeight:'500', fontSize: 16}}>Durgesh Kumar</BoldText>
                  <BoldText style={{ fontWeight:'400', fontSize: 14}}>Class: <LightText>2</LightText></BoldText>
                  <BoldText style={{ fontWeight:'400', fontSize: 14}}>Roll No.: <LightText>12</LightText></BoldText>
                  <LightText style={{ fontWeight:'400', fontSize: 14}}>2020-2021</LightText>
                </View>
              </TouchableOpacity>
            </View>
          <ScrollView style={{ flex: 1}}>  
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'space-evenly', backgroundColor:'rgb(222,221,219)' }}>
              {/* 1st row */}
              <View style={{ flex: 1, paddingHorizontal: 20, flexDirection: 'row' }}>
                <TouchableOpacity style={{ flex: 1, padding: 15, justifyContent: 'center', }}
                 onPress={() => this.props.navigation.navigate('Attendance')}
                >
                  <View style={{ flex: 1, paddingVertical: 15, alignItems: 'center', justifyContent: 'space-evenly', backgroundColor: '#fff', borderRadius: 5 }}>
                    <View style={{ height: (width / 5), width: (width / 5), borderRadius: (width / 5) / 2, alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgb(255, 124, 49)' }}>
                      <Image
                        source={require('../assets/attendance.png')}
                        style={{ height: (width / 8), width: (width / 8) }}
                      />
                    </View>
                    <LightText style={{ fontSize: 16, textAlign: 'center', color: '#ff732e' }} >Attendance</LightText>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity style={{ flex: 1, padding: 15, justifyContent: 'center' }}
                // onPress={() => this.props.navigation.navigate('PDFView', { url: 'Advisory.pdf', title: 'Advisory' })}
                >
                  <View style={{ flex: 1,paddingVertical: 15, alignItems: 'center', justifyContent: 'space-evenly', backgroundColor: '#fff', borderRadius: 5 }}>
                    <View style={{ height: (width / 5), width: (width / 5), borderRadius: (width / 5) / 2, alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgb(255, 124, 49)' }}>
                      <Image
                        source={require('../assets/time-management.png')}
                        style={{ height: (width / 8), width: (width / 8) }}
                      />
                    </View>
                    <LightText style={{ fontSize: 16, textAlign: 'center', color: '#ff732e' }} >Timetable</LightText>
                  </View>
                </TouchableOpacity>
              </View>

              {/* 2nd row */}
              <View style={{ flex: 1, paddingHorizontal: 20, flexDirection: 'row' }}>
                <TouchableOpacity style={{ flex: 1, padding: 15, justifyContent: 'center', }}
                // onPress={() => this.props.navigation.navigate('AboutNew')}
                >
                  <View style={{ flex: 1,paddingVertical: 15, alignItems: 'center', justifyContent: 'space-evenly', backgroundColor: '#fff', borderRadius: 5 }}>
                    <View style={{ height: (width / 5), width: (width / 5), borderRadius: (width / 5) / 2, alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgb(255, 124, 49)' }}>
                      <Image
                        source={require('../assets/calendar.png')}
                        style={{ height: (width / 8), width: (width / 8) }}
                      />
                    </View>
                    <LightText style={{ fontSize: 16, textAlign: 'center', color: '#ff732e' }} >Calendar</LightText>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity style={{ flex: 1, padding: 15, justifyContent: 'center' }}
                // onPress={() => this.props.navigation.navigate('PDFView', { url: 'Advisory.pdf', title: 'Advisory' })}
                >
                  <View style={{ flex: 1,paddingVertical: 15, alignItems: 'center', justifyContent: 'space-evenly', backgroundColor: '#fff', borderRadius: 5 }}>
                    <View style={{ height: (width / 5), width: (width / 5), borderRadius: (width / 5) / 2, alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgb(255, 124, 49)' }}>
                      <Image
                        source={require('../assets/homework.png')}
                        style={{ height: (width / 8), width: (width / 8) }}
                      />
                    </View>
                    <LightText style={{ fontSize: 16, textAlign: 'center', color: '#ff732e' }} >Home Work</LightText>
                  </View>
                </TouchableOpacity>
              </View>
              {/* 3rd row */}
              <View style={{ flex: 1, paddingHorizontal: 20, flexDirection: 'row' }}>
                <TouchableOpacity style={{ flex: 1, padding: 15, justifyContent: 'center', }}
                // onPress={() => this.props.navigation.navigate('AboutNew')}
                >
                  <View style={{ flex: 1,paddingVertical: 15, alignItems: 'center', justifyContent: 'space-evenly', backgroundColor: '#fff', borderRadius: 5 }}>
                    <View style={{ height: (width / 5), width: (width / 5), borderRadius: (width / 5) / 2, alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgb(255, 124, 49)' }}>
                      <Image
                        source={require('../assets/bus.png')}
                        style={{ height: (width / 8), width: (width / 8) }}
                      />
                    </View>
                    <LightText style={{ fontSize: 16, textAlign: 'center', color: '#ff732e' }} >Transport</LightText>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity style={{ flex: 1, padding: 15, justifyContent: 'center' }}
                // onPress={() => this.props.navigation.navigate('PDFView', { url: 'Advisory.pdf', title: 'Advisory' })}
                >
                  <View style={{ flex: 1, paddingVertical: 15, alignItems: 'center', justifyContent: 'space-evenly', backgroundColor: '#fff', borderRadius: 5 }}>
                    <View style={{ height: (width / 5), width: (width / 5), borderRadius: (width / 5) / 2, alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgb(255, 124, 49)' }}>
                      <Image
                        source={require('../assets/art.png')}
                        style={{ height: (width / 8), width: (width / 8) }}
                      />
                    </View>
                    <LightText style={{ fontSize: 16, textAlign: 'center', color: '#ff732e' }} >Gallery</LightText>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </ScrollView>
        </View>
      </View>
    )
  }
}
