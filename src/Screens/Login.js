import React, { Component } from "react";
import {
  KeyboardAvoidingView,
  StyleSheet,
  Image,
  View,
  Dimensions,
  TouchableWithoutFeedback,
  Keyboard,
  ImageBackground
} from "react-native";
import {
  LightText,
  BoldText,
  TextInputField
} from '../Components/styledTexts';
import Loader from '../Components/Loader';
import { Button } from '../Components/button';
import colors from "../styles/colors";
import Header from '../Components/Header';

let { height, width } = Dimensions.get("window");

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLogin: true,
    }
  }

  componentWillMount() {
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <ImageBackground
          source={require('../assets/backgrounds/bg2.png')}
          style={{ flex: 1, width: width }}
        >
          <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
            <KeyboardAvoidingView contentContainerStyle={{ flex: 1 }}
              style={{ height: height, width: width }}
              behavior='position' enabled
              keyboardVerticalOffset={-200}
            >
              <View style={{ flex: 1 }}>
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                  <Image
                    source={require('../assets/stu.png')}
                    style={{ tintColor: '#fff', height: 60, width: 60 }}
                  />
                  <LightText style={{ color: '#fff', fontSize: 20 }}>Sign In</LightText>
                </View>
                <View style={{ flex: 1, justifyContent: 'center', paddingHorizontal: 20 }}>
                  <TextInputField
                    containerStyle={{ backgroundColor: 'rgba(255, 255, 255, 0.52)' }}
                    placeholder={'User name'}
                    image={require('../assets/user.png')}
                  />
                  <TextInputField
                    containerStyle={{ marginTop: 20, backgroundColor: 'rgba(255, 255, 255, 0.52)' }}
                    placeholder={'Password'}
                    image={require('../assets/lock.png')}
                    secureTextEntry={true}
                  />
                </View>
                <View style={{ flex: 1 }}>
                  <Button
                    color={colors.buttonColor}
                    value={'Sign In'}
                    onPress={()=> this.props.navigation.navigate('drawerNavigation')}
                  />
                  <BoldText style={{ textAlign: 'center', color: '#fff', marginVertical: 10 }}>Forgot Password?</BoldText>
                  <LightText style={{ textAlign: 'center', color: '#fff' }} onPress={() => this.props.navigation.navigate('SignUp')} >Sign Up</LightText>

                </View>
              </View>
            </KeyboardAvoidingView>
          </TouchableWithoutFeedback>
        </ImageBackground>
        <Loader loading={this.props.loading} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  normalInput: {
    height: 40,
    color: 'rgb(119,120,122)',
    marginLeft: 10,
    flex: 1
  },
  errorInput: {
    borderColor: 'red'
  }
});
