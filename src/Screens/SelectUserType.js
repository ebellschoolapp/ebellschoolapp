import React, { Component } from "react";
import {
  Dimensions,
  Image,
  View,
  ImageBackground
} from "react-native";
import {
  LightText,
  BoldText
} from '../Components/styledTexts';
import Loader from '../Components/Loader';
import { Button } from '../Components/button';
import colors from "../styles/colors";
import Header from '../Components/Header';

let { height, width } = Dimensions.get("window");

export default class SelectUserType extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLogin: true,
    }
  }

  componentWillMount() {
  }
  render() {
    return (
      <View style={{ flex: 1 }}>
        <ImageBackground
          source={require('../assets/bg1.png')}
          style={{ flex: 1, width: width }}
        >
          <View style={{ flex: 1 }}>
            <View style={{ flex: .3, alignItems: 'center', justifyContent: 'center' }}>
              <Image
                source={require('../assets/stu.png')}
                style={{ tintColor: '#fff', height: 60, width: 60 }}
              />
            </View>
            <View style={{ flex: 1, justifyContent: 'center', }}>
              <Button
                color={colors.buttonColor}
                value={'Teacher'}
                onPress={() => this.props.navigation.navigate('loginNavigationOptions')}
              />
              <Button
                color={colors.buttonColor}
                value={'Student'}
                onPress={() => this.props.navigation.navigate('loginNavigationOptions')}
              />
              <Button
                color={colors.buttonColor}
                value={'Admin'}
                onPress={() => this.props.navigation.navigate('loginNavigationOptions')}
              />
            </View>
            <View style={{ justifyContent: 'center' }}>
              <BoldText style={{ textAlign: 'center', color: '#fff', }}>Welcome</BoldText>
              <BoldText style={{ textAlign: 'center', color: '#ff732e', marginVertical: 20 }}>My school app</BoldText>
            </View>
          </View>
        </ImageBackground>
      </View>
    )
  }

}