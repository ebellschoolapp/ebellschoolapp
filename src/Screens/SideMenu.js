import PropTypes from 'prop-types';
import React, {Component} from 'react';
import {NavigationActions} from 'react-navigation';
import { DrawerActions } from 'react-navigation-drawer';
import { ScrollView, Text, View, SafeAreaView, Image} from 'react-native';
import {BoldText, LightText} from '../Components/styledTexts';
import { TouchableOpacity } from 'react-native-gesture-handler';
class SideMenu extends Component {

  constructor(props){
    super(props);
   this.state = {
   }
}

  navigateToScreen = (route) => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
  }

  render () {
    return (
      <View style={{flex: 1, backgroundColor:'rgb(238,121,80)'}}>
        <ScrollView style={{ flex: 1 }}>
        <View style={{ flex: 1, alignItems:'center' }}>
                    <Image
                        source = { require('../assets/user.png')}
                         style={{ marginTop: 100, height: 100, width: 100 , borderRadius: 50}}
                        resizeMode={'contain'}
                    />
                    <BoldText style={{ color:'#fff'}}>Name</BoldText>
                </View> 
        <View style={{ flex: 3, marginHorizontal: 15 }}>
          <TouchableOpacity style={{ marginVertical:10}}
            onPress={()=> this.props.navigation.drawer.dispatch(DrawerActions.toggleDrawer())}
          >
        <BoldText style={{ color:'#fff'}}>Profile</BoldText>
        </TouchableOpacity>
        <TouchableOpacity style={{ marginVertical:10}}
          onPress={()=> this.props.navigation.drawer.dispatch(DrawerActions.toggleDrawer())}
        >
        <BoldText style={{ color:'#fff'}}>Message</BoldText>
        </TouchableOpacity>
        <TouchableOpacity style={{ marginVertical:10}}
          onPress={()=> this.props.navigation.drawer.dispatch(DrawerActions.toggleDrawer())}
        >
        <BoldText style={{ color:'#fff'}}>Courses</BoldText>
        </TouchableOpacity>
        <TouchableOpacity style={{ marginVertical:10}}
          onPress={()=> this.props.navigation.drawer.dispatch(DrawerActions.toggleDrawer())}
        >
        <BoldText style={{ color:'#fff'}}>Exams</BoldText>
        </TouchableOpacity>
        <TouchableOpacity style={{ marginVertical:10}}
          onPress={()=> this.props.navigation.drawer.dispatch(DrawerActions.toggleDrawer())}
        >
        <BoldText style={{ color:'#fff'}}>Settings</BoldText>
        </TouchableOpacity>
        <TouchableOpacity style={{ marginVertical:10}}
          onPress={()=> this.props.navigation.drawer.dispatch(DrawerActions.toggleDrawer())}
        >
        <BoldText style={{ color:'#fff'}}>Sign Out</BoldText>
        </TouchableOpacity>

          </View>
        </ScrollView>
      </View>
    );
  }
}

SideMenu.propTypes = {
  navigation: PropTypes.object
};

export default SideMenu;