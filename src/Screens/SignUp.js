import React, { Component } from "react";
import {
  KeyboardAvoidingView,
  StyleSheet,
  View,
  Dimensions,
  Image,
  TouchableWithoutFeedback,
  Keyboard,
  ImageBackground
} from "react-native";
import {
  LightText,
  BoldText,
  TextInputField
} from '../Components/styledTexts';
import Loader from '../Components/Loader';
import { Button } from '../Components/button';
import colors from "../styles/colors";
import Header from '../Components/Header';

let { height, width } = Dimensions.get("window");

export default class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLogin: true,
    }
  }

  componentWillMount() {
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <ImageBackground
          source={require('../assets/backgrounds/loginbg.png')}
          style={{ flex: 1, width: width }}
        >
          <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
            <KeyboardAvoidingView contentContainerStyle={{ flex: 1 }}
              style={{ height: height, width: width }}
              behavior='position' enabled
              keyboardVerticalOffset={-200}
            >
              <View style={{ flex: 1 }}>
              <View style={{ flex: 1, alignItems:'center', justifyContent:'center'}}>
                <LightText style={{ color:'#fff', fontSize:20 }}>Sign Up</LightText>
                <Image source={require('../assets/admin-settings-male.png')}
                    style={{ tintColor:'#fff' }}
                />
              </View>
              <View style={{ flex: 1, justifyContent:'center', paddingHorizontal: 20 }}>
                  <TextInputField
                    containerStyle={{ backgroundColor: 'rgba(255, 255, 255, 0.52)' }}
                    placeholder={'Name'}
                    image={require('../assets/user.png')}
                  />
                  <TextInputField
                      containerStyle={{ marginTop:20, backgroundColor: 'rgba(255, 255, 255, 0.52)'}}
                      placeholder={'Email'}
                      image={require('../assets/mail.png')}
                  />
                  <TextInputField
                      containerStyle={{ marginTop:20, backgroundColor: 'rgba(255, 255, 255, 0.52)'}}
                      placeholder={'Phone'}
                      image={require('../assets/Phone.png')}
                  />
                  <TextInputField
                      containerStyle={{ marginTop:20, backgroundColor: 'rgba(255, 255, 255, 0.52)'}}
                      placeholder={'Password'}
                      image={require('../assets/lock.png')}
                      secureTextEntry={true}
                  />
              </View>
              <View style={{ flex: .8 }}>
                      <Button
                        color={colors.buttonColor}
                        value={'Sign Up'}
                        onPress={()=> this.props.navigation.navigate('drawerNavigation')}
                    />
                    <LightText style={{ textAlign:'center', color:'#fff'}} onPress={()=> this.props.navigation.goBack()} >Sign In</LightText>
              </View>
              </View>
            </KeyboardAvoidingView>
          </TouchableWithoutFeedback>
        </ImageBackground>
        <Loader loading={this.props.loading} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  normalInput: {
    height: 40,
    color: 'rgb(119,120,122)',
    marginLeft: 10,
    flex: 1
  },
  errorInput: {
    borderColor: 'red'
  }
});
