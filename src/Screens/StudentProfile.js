import React, { Component } from "react";
import {
    View,
    TouchableOpacity,
    Dimensions,
    Image,
    ScrollView
} from "react-native";
import Loader from '../Components/Loader';
import { BoldText, LightText, MediumText } from '../Components/styledTexts';
import Header from '../Components/Header';
import colors from "../styles/colors";

const { height, width } = Dimensions.get('screen');

export default class StudentProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    render() {
        return (<View style={{ flex: 1, }}>
            <View style={{ height: 60 }}>
                <Header
                    leftNavigation={this.props.navigation}
                    value={'Student Profile'}
                />
            </View>
            <ScrollView style={{ flex: 1}}>
            <View style={{ margin: 10, padding: 10,borderRadius: 5,  backgroundColor:'rgb(222,221,219)'}}>
            <View style={{ flexDirection: 'row', justifyContent: 'center' }} >
                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <Image
                    source={require('../assets/user.png')}
                    style={{ height: 100, width: 100, borderRadius: 50, borderWidth: 1 }}
                  />
                </View>
                <View style={{ flex: 1, marginLeft: 15 }}>
                  <BoldText style={{ fontWeight:'500', fontSize: 16}}>Durgesh Kumar</BoldText>
                  <BoldText style={{ fontWeight:'500', fontSize: 14, marginVertical: 5}}>Roll no:  <LightText>DPSv02</LightText></BoldText>
                  <BoldText style={{ fontWeight:'500', fontSize: 14}}>Father:  <LightText>Mr. Ram</LightText></BoldText>
                  <BoldText style={{ fontWeight:'500', fontSize: 14, marginVertical: 5}}>Mother:  <LightText>Mrs. Sheeta</LightText></BoldText>
                  <BoldText style={{ fontWeight:'500', fontSize: 14, }}>Contact:  <LightText>8802330095</LightText></BoldText>
                  <BoldText style={{ fontWeight:'500', fontSize: 14, marginVertical: 5}}>Class:  <LightText>IV (A)</LightText></BoldText>
                </View>
              </View>
              <View style={{ height: 1, marginVertical: 10, backgroundColor: colors.gray}}/>
              <View style={{ flexDirection:'row',  justifyContent:'space-between'}}>
                
                <BoldText style={{ fontWeight:'500', fontSize: 14}}>School: </BoldText>
                <LightText style={{ flex: 1}}><BoldText style={{ fontWeight:'500', fontSize: 16}}>Delhi public school Delhi</BoldText> {`\nSite No I, Uday nagar, Road, Urban Estate, Sector 45, Gurugram, Haryana 122001`}</LightText>
                <Image
                    source={require('../assets/user.png')}
                    style={{ height: 40, width: 40, borderRadius: 50, borderWidth: 1 }}
                  />
                </View>
              </View>

              <View style={{ marginHorizontal: 10, padding: 10,borderRadius: 5, alignItems:'center',  backgroundColor:'rgb(222,221,219)'}}>
                <MediumText style={{ fontWeight:'600', fontSize: 16, textAligin:'center'}}>Class Teacher</MediumText>
                <View style={{ flexDirection:'row', marginTop: 10}}>
                    <View style={{ flex: 1}}>
                    <BoldText style={{ fontWeight:'500', fontSize: 14, marginVertical: 5}}>Name:   <LightText>Pawan Singh</LightText></BoldText>
                    <BoldText style={{ fontWeight:'500', fontSize: 14, marginVertical: 5}}>Phone:  <LightText>9999999999</LightText></BoldText>
                    <BoldText style={{ fontWeight:'500', fontSize: 14, marginVertical: 5}}>Email:   <LightText>abc@gmail.com</LightText></BoldText>
                    </View>
                    <Image
                    source={require('../assets/user.png')}
                    style={{ height: 60, width: 60, borderRadius: 50, borderWidth: 1 }}
                  />
                </View>
                
                </View>

                <View style={{ marginHorizontal: 10, marginVertical: 10, padding: 10,borderRadius: 5, alignItems:'center',  backgroundColor:'rgb(222,221,219)'}}>
                <MediumText style={{ fontWeight:'600', fontSize: 16, textAligin:'center'}}>Other Detail</MediumText>
                <View style={{ flexDirection:'row', marginTop: 10}}>
                    <View style={{ flex: 1}}>
                    <BoldText style={{ fontWeight:'500', fontSize: 14, marginVertical: 5}}>Date of birth:       <LightText>06 Jun 1991</LightText></BoldText>
                    <BoldText style={{ fontWeight:'500', fontSize: 14, marginVertical: 5}}>Gender:                 <LightText>Male</LightText></BoldText>
                    <BoldText style={{ fontWeight:'500', fontSize: 14, marginVertical: 5}}>Category:              <LightText>General</LightText></BoldText>
                    <BoldText style={{ fontWeight:'500', fontSize: 14, marginVertical: 5}}>Total present:       <LightText>20 Days</LightText></BoldText>
                    <BoldText style={{ fontWeight:'500', fontSize: 14, marginVertical: 5}}>Admission Date:   <LightText>06 Jun 2020</LightText></BoldText>

                    </View>
                    
                </View>
                
                </View>

            </ScrollView>
        </View>)
    }
}