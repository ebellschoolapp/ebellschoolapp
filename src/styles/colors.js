export default {
  backGroundGradient:['#FFBD4B', '#FFBD4B', '#F6572F'],
    buttonColor:'#f56105',
    white:'#FFF',
    black: "#000000",
    appColor: '#F38C33',
    gray: '#9B9B9B',
    lightGray: '#E8E8E8',
    strongRed:'#C94C17',
    charcoal:'#444444',
    plum:'#7B2271'
  };
  